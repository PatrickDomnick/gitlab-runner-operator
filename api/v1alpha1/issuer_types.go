/*
Copyright 2023.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1alpha1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// EDIT THIS FILE!  THIS IS SCAFFOLDING FOR YOU TO OWN!
// NOTE: json tags are required.  Any new fields you add must have json tags for the fields to be serialized.

// IssuerSpec defines the desired state of Issuer
type IssuerSpec struct {
	// INSERT ADDITIONAL SPEC FIELDS - desired state of cluster
	// Important: Run "make" to regenerate code after modifying this file

	// AccessKeySecret is the Name of the Secret where the Gitlab Access Token is stored.
	// The Access Token itself (not this value) should look something like this: glpat-
	AccessTokenSecret string `json:"accessTokenSecret,omitempty"`

	// AccessTokenKey is the Key of the Secret where the Gitlab Access Token is stored.
	// Default: token
	AccessTokenKey string `json:"accessTokenKey,omitempty"`

	// GitlabApiUrl is an field of Credential. Edit credential_types.go to remove/update
	// Default: "https://gitlab.com/api/v4"
	GitlabApiUrl string `json:"gitlabApiUrl,omitempty"`
}

// IssuerStatus defines the observed state of Issuer
type IssuerStatus struct {
	// INSERT ADDITIONAL STATUS FIELD - define observed state of cluster
	// Important: Run "make" to regenerate code after modifying this file
	Conditions []metav1.Condition `json:"conditions,omitempty" patchStrategy:"merge" patchMergeKey:"type" protobuf:"bytes,1,rep,name=conditions"`
}

//+kubebuilder:object:root=true
//+kubebuilder:subresource:status

// Issuer is the Schema for the issuers API
// +kubebuilder:printcolumn:name="Access Token Secret",type="string",JSONPath=".spec.accessTokenSecret",description="The name of the secret where the access key is stored"
// +kubebuilder:printcolumn:name="Access Token Key",type="string",JSONPath=".spec.accessTokenKey",description="The Key of the Secret where the Gitlab Access Token is stored"
// +kubebuilder:printcolumn:name="Status",type="string",JSONPath=".status.conditions[?(@.type==\"Issuer\")].reason",description="The status of this resource"
type Issuer struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   IssuerSpec   `json:"spec,omitempty"`
	Status IssuerStatus `json:"status,omitempty"`
}

//+kubebuilder:object:root=true

// IssuerList contains a list of Issuer
type IssuerList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []Issuer `json:"items"`
}

func init() {
	SchemeBuilder.Register(&Issuer{}, &IssuerList{})
}
